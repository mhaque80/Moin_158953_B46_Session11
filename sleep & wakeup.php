<?php
class Person{

    public $a="a123";
    public $b="a456";
    public $c="a789";

    public function __sleep()
    {
     return array("a","c");
    }
    public function __wakeup()
    {
        return array("c");
    }
    public function __toString()
    {
        return "Are you  craze? I'm an object<br>";
    }
    public function __invoke()
    {
        echo "I am inside invoke function<br>";
    }

}

$objPerson=new Person();
$myVar=serialize($objPerson);
var_dump($myVar);

print_r(unserialize($myVar));

echo $objPerson;
$objPerson();
